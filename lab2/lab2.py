import hashlib
import threading

hash = hashlib.sha256()
array = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
         'w', 'x', 'y', 'z']

checker = ['No', 'No', 'No']

def hash_one(word):
    if hashlib.sha256(word.encode('ascii',
                                  'ignore')).hexdigest() == '1115dd800feaacefdf481f1f9070374a2a81e27880f187396db67958b207cbad':
        print(f'Первое слово: {word}')
        checker[0] = 'True'
        return checker

def hash_two(word):
    if hashlib.sha256(word.encode('ascii',
                                  'ignore')).hexdigest() == '3a7bd3e2360a3d29eea436fcfb7e44c735d117c42d1c1835420b6b9942dd4f1b':
        print(f'Второе слово: {word}')
        checker[1] = 'True'
        return checker

def hash_three(word):
    if hashlib.sha256(word.encode('ascii',
                                  'ignore')).hexdigest() == '74e1bb62f8dabb8125a58852b63bdf6eaef667cb56ac7f7cdba6d7305c50a22f':
        print(f'Третье слово: {word}')
        checker[2] = 'True'
        return checker

def dictionary(calc, amount):
    global hash_two_flag
    for i1 in range(amount):
        for i2 in range(amount):
            for i3 in range(amount):
                for i4 in range(amount):
                    for i5 in range(amount):
                        word = array[i1] + array[i2] + array[i3] + array[i4] + array[i5]
                        if checker[0] != 'True':
                            hash_one(word)
                        if checker[1] != 'True':
                            hash_two(word)
                        if checker[2] != 'True':
                            hash_three(word)

def threaded(theads, calc, amount):
    threads = []
    for thead in range(theads):
        t1 = threading.Thread(target=dictionary, args=(calc, amount))
        threads.append(t1)
        t1.start()

    for t1 in threads:
        t1.join()


if __name__ == '__main__':
    threaded(6, 20, len(array))
