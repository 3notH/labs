import time

def function(i, b, c):
    if i == 0:
        return 0
    a = 0
    for n in range(1, 100000001):
        a += b * 2 + c - i
    return function(i-1, b, c) + a

i = 10
b = 20
c = 30
start_time = time.monotonic()
result = function(i, b, c)
print("Время: " + str(time.monotonic() - start_time))
print("Ответ: " + str(result))
