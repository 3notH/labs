import os
def remove_file(name):
    os.remove(name)

#Вывод инфорации о логических дисках, именах, метке тома, размере и типе файловой системы
import win32api
def info_os():
    print(win32api.GetLogicalDriveStrings().split("\x00")[:-1])

#2.
def file():
    file = open('file.txt', 'w')
    print('Файл file.txt создан и готов для использования')
    file_read = open('file.txt')
    file.write(input('Введите текст, который позже будет сохранен в файл: '))
    file.close()
    print(file_read.read())
    file_read.close()
    remove_file("file.txt")

#3.
import json
def json():
    dictionary = {
        'a' : 34,
        'b' : 61,
        'c' : 345
    }
    with open('data.json', 'w') as write_file:
        json.dump(dictionary, write_file)
    with open('data.json', 'r') as read_json:
        print(json.load(read_json))
        remove_file("data.json")

#4.
import xml.etree.ElementTree as ET
def xml():
    element = ET.Element('buttom')
    property = ET.SubElement(element, 'color')
    tree = ET.ElementTree(element)
    tree.write("buttom.xml")
    ET.dump(element)
    remove_file("buttom.xml")


#5
from pathlib import Path
import zipfile
from zipfile import ZipFile
def zip():
    with ZipFile("archive.zip", "w") as newzip:
        newzip.write("file1.txt")
        newzip.write("file2.txt")
        remove_file("file1.txt")
        remove_file("file2.txt")

    zip_r = zipfile.ZipFile("archive.zip")
    zip_r.extract("file1.txt")
    zip_r.close()
    print(Path("file1.txt").stat())

if __name__ == '__main__':
    info_os()
#    file()
#    json()
#    xml()
#    zip()
